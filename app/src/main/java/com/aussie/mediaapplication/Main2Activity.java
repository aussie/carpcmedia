package com.aussie.mediaapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.aussie.mediaapplication.core.MediaService;
import com.aussie.mediaapplication.core.MediaTrackItem;
import com.aussie.mediaapplication.utils.ReadNetworkData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

public class Main2Activity extends AppCompatActivity implements ReadNetworkData.IDataReady {
    MediaService mediaService;

    private Main2Activity activity = this;
    private SurfaceView surfaceView;
    private SeekBar seekBar;

    private String contentData;
    private String csrfToken;
    private String param_d_id;
    private String param_video_token;
    private String param_access_key;
    private String iframeUrl;
    private String videoFileName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mediaService = MediaService.getInstance(getBaseContext());
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        seekBar = (SeekBar) findViewById(R.id.seekBar3);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReadNetworkData reader = new ReadNetworkData();
                reader.setOnDataReadyEvent(activity);

//                reader.requestData("http://www.hdkinoteatr.com/series/2410-interny.html", 1);
//                reader.requestData("http://www.hdkinoteatr.com/sci-fi/21488-marsianin.html", 1);
                reader.requestData("http://www.hdkinoteatr.com/thriller/22618-shpionskiy-most.html", 1);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser && mediaService.isPlaying()) {
                    mediaService.seek(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public void onDataReady(String data, Object sender) {
        switch ((int) sender) {
            case 1: {
                // get url to iframe
                String vkArrayTemplate = "(.*var vkArr=)(.*?)(;.*)";

                if (data.matches(vkArrayTemplate)) {
                    String tmp = data.replaceFirst(vkArrayTemplate, "$2");

                    if (tmp.matches("^\\[\\{.*")) {
                        iframeUrl = tmp.replaceFirst("(^\\[\\{.*?)(http://)(.*?)(iframe)(.*)", "$2$3$4");

                        ReadNetworkData reader = new ReadNetworkData();
                        reader.setOnDataReadyEvent(activity);

                        reader.requestData(iframeUrl, 2);
                    } else if (tmp.matches("^['].*")) {
                        System.out.println("serial");
                    }
                }

                break;
            }
            case 2: {
                String keyTokenTemplate = "(.*[|]{2}+setRequestHeader[|])(.*?)([|].*)";
                String videoCredentialsTemplate = "(.*[$].post[(]'[/]sessions[/]create_session',\\s*+[{]\\s*+)(.*?)(\\s*+[}].*)";
                String csrfTokenTemplate = "(.*\"csrf-token\" content=\")(.*?)(\".*)";
                String videoFileNameTemplate = "(.*player_osmf[(]')(.*?)('.*)";

                String keyToken = data.replaceFirst(keyTokenTemplate, "$2");
                String videoParams = data.replaceFirst(videoCredentialsTemplate, "$2");
                csrfToken = data.replaceFirst(csrfTokenTemplate, "$2");
                videoFileName = data.replaceFirst(videoFileNameTemplate, "$2");

                for (String param : videoParams.split(",")) {
                    if (param.trim().startsWith("d_id")) {
                        String tmp[] = param.split(":");
                        param_d_id = tmp[1].trim();
                    } else if (param.trim().startsWith("video_token")) {
                        String tmp[] = param.split(":");
                        param_video_token = tmp[1].trim().replaceAll("'", "");
                    } else if (param.trim().startsWith("access_key")) {
                        String tmp[] = param.split(":");
                        param_access_key = tmp[1].trim().replace("'", "");
                    }
                }

                contentData = encode_ContentData(keyToken);

                ReadNetworkData reader = new ReadNetworkData();
                reader.setOnDataReadyEvent(activity);

                Map<String, String> headerParams = new HashMap<>();
                headerParams.put("X-Requested-With", "XMLHttpRequest");
                headerParams.put("X-CSRF-Token", csrfToken);
                headerParams.put("Content-Data", contentData);

//                headerParams.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//                headerParams.put("Referer", iframeUrl);
//                headerParams.put("Connection", "keep-alive");
//                headerParams.put("Host", "moon.hdkinoteatr.com");
//
                Map<String, String> params = new HashMap<>();
                params.put("video_token", param_video_token);
                params.put("stat_type", "2");
                params.put("stat_value", "detected_false");

                reader.requestDataByPost("http://moon.hdkinoteatr.com/stats/send_data_v1", 3, headerParams, params);
                break;
            }
            case 3: {
                ReadNetworkData reader = new ReadNetworkData();
                reader.setOnDataReadyEvent(activity);

                Map<String, String> headerParams = new HashMap<>();
                headerParams.put("X-Requested-With", "XMLHttpRequest");
                headerParams.put("X-CSRF-Token", csrfToken);
                headerParams.put("Content-Data", contentData);

//                headerParams.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//                headerParams.put("Referer", iframeUrl);
//                headerParams.put("Connection", "keep-alive");
//                headerParams.put("Host", "moon.hdkinoteatr.com");

                Map<String, String> params = new HashMap<>();
                params.put("video_token", param_video_token);
                params.put("stat_type", "4");
                params.put("stat_value", "adv_600x300_try");

                reader.requestDataByPost("http://moon.hdkinoteatr.com/stats/send_data_v1", 4, headerParams, params);
                break;
            }

            case 4: {
                ReadNetworkData reader = new ReadNetworkData();
                reader.setOnDataReadyEvent(activity);

                Map<String, String> headerParams = new HashMap<>();
                headerParams.put("X-Requested-With", "XMLHttpRequest");
                headerParams.put("X-CSRF-Token", csrfToken);
                headerParams.put("Content-Data", contentData);


//                headerParams.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//                headerParams.put("Referer", iframeUrl);
//                headerParams.put("Connection", "keep-alive");
//                headerParams.put("Host", "moon.hdkinoteatr.com");


                Map<String, String> params = new HashMap<>();
                params.put("d_id", param_d_id);
                params.put("video_token", param_video_token);
                params.put("content_type", "movie");
                params.put("access_key", param_access_key);
                params.put("cd", "0");
                params.put("partner", "");

                reader.requestDataByPost("http://moon.hdkinoteatr.com/sessions/create_session", 5, headerParams, params);
                break;
            }
            case 5: {
                try {
                    JSONObject playLists = new JSONObject(data);

                    String manifest_f4m_url = playLists.getString("manifest_f4m");
                    String manifest_m3u8_url = playLists.getString("manifest_m3u8");
                    String manifest_dash_url = playLists.getString("manifest_dash");

                    ReadNetworkData reader = new ReadNetworkData();
                    reader.setOnDataReadyEvent(activity);
                    reader.setRequestParams(null);
//                    reader.requestData(manifest_f4m_url, 6);
                    reader.requestData(manifest_m3u8_url, 6);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            }

            case 6: {
                String[] streams = data.split("#");
                String streamUrl = streams[streams.length - 1].replaceFirst(".*BANDWIDTH=\\d++", "");
                String[] resolution = streams[streams.length - 1].replaceFirst("(.*RESOLUTION=)(.*?)([,].*)", "$2").split("x");


                try {
                    mediaService.playStreamVideo(streamUrl, surfaceView, Integer.parseInt(resolution[0]), Integer.parseInt(resolution[1]), 0);
                    mediaService.setUpdatePosition(new MediaService.UpdatePosition() {
                        @Override
                        public void onPositionChanged(int duration, int currentPosition) {
                            seekBar.setMax(duration);
                            seekBar.setProgress(currentPosition);
                        }

                        @Override
                        public void onTrackChanged(int trackPosition, MediaTrackItem track) {

                        }
                    });
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(), "ERROR: " + e.toString(), Toast.LENGTH_LONG).show();
                }

                break;
            }
        }


//        [{"comment": "Фильм", "file": "http://moon.hdkinoteatr.com/video/2ce59dc877967c89/iframe"},{"comment": "Трейлер", "file": "oid=16437975&id=161720982&hash=2988e5b2fa68741f&hd=2"}]
        System.out.println(data);
    }


    void writeFile(String fileName, String data) {
        try {
            // отрываем поток для записи
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(openFileOutput(fileName, MODE_PRIVATE)));
            // пишем данные
            bw.write(data);
            // закрываем поток
            bw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String encode_ContentData(String e) {
        String _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

        String result = "";
        int n, r, i, s, o, u, a;
        int f = 0;
        e = _utf8_encode(e);
        while (f < e.length()) {
            n = e.charAt(f++);
            if (f == e.length()) {
                r = 0;
                i = 0;
            } else {
                r = e.charAt(f++);
                i = e.charAt(f++);
            }
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (r == 0) {
                u = a = 64;
            } else if (i == 0) {
                a = 64;
            }
            result = result + _keyStr.charAt(s) + _keyStr.charAt(o) + _keyStr.charAt(u) + _keyStr.charAt(a);
        }
        return result;
    }

    private String _utf8_encode(String e) {
        e = e.replaceAll("\\r\\n", "\\n");
        StringBuilder t = new StringBuilder();
        for (int n = 0; n < e.length(); n++) {
            char r = e.charAt(n);
            if (r < 128) {
                t.append(r);
            } else if (r > 127 && r < 2048) {
                t.append(r >> 6 | 192);
                t.append(r & 63 | 128);
            } else {
                t.append(r >> 12 | 224);
                t.append(r >> 6 & 63 | 128);
                t.append(r & 63 | 128);
            }
        }

        return t.toString();
    }
}
