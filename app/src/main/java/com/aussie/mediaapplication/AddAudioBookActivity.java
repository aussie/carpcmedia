package com.aussie.mediaapplication;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.aussie.mediaapplication.core.MediaTrackItem;
import com.aussie.mediaapplication.core.abooks.ASBookReader;
import com.aussie.mediaapplication.core.abooks.AudioknigiClubReader;
import com.aussie.mediaapplication.core.abooks.IBookReader;
import com.aussie.mediaapplication.core.abooks.KnigogolikReader;
import com.aussie.mediaapplication.core.adapters.ABookReadersAdapter;
import com.aussie.mediaapplication.utils.DBHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddAudioBookActivity extends AppCompatActivity {
    private AddAudioBookActivity currentActivity = this;

    private Button btnAddBook;
    private EditText urlEdit;
    private ListView readersList;
    private List<IBookReader> readers = new ArrayList<>();

    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_audio_book);

        dbHelper = new DBHelper(getBaseContext());

        btnAddBook = (Button) findViewById(R.id.btnAddBook);
        urlEdit = (EditText) findViewById(R.id.url);
        readersList = (ListView) findViewById(R.id.readersList);

        readers.add(new ASBookReader());
        readers.add(new AudioknigiClubReader());
        readers.add(new KnigogolikReader());

        ABookReadersAdapter adapter = new ABookReadersAdapter(getBaseContext(), readers);
        readersList.setAdapter(adapter);

        readersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ABookReadersAdapter adapter = ((ABookReadersAdapter) parent.getAdapter());
                adapter.setCurrentIdx(position);
                adapter.notifyDataSetChanged();
            }
        });

        btnAddBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IBookReader reader = ((ABookReadersAdapter) readersList.getAdapter()).getCurrentReader();
                final String url = urlEdit.getText().toString();

                reader.setOnDataReady(new IBookReader.IPlaylistDataReady() {
                    @Override
                    public void onPlaylistDataReady(String bookName, String bookGenre, String bookDescription, String bookCoverUrl, List<MediaTrackItem> playlist) {
                        SQLiteDatabase db = dbHelper.getWritableDatabase();

                        try {
                            db.execSQL("DELETE FROM abook_tracks WHERE abook_id in (SELECT id FROM abooks WHERE book_name = '" + bookName + "')");
                            db.execSQL("DELETE FROM abooks WHERE book_name = '" + bookName + "'");
                        } catch (Exception e) {
                            System.out.println(e);
                        }

                        // create the book values object
                        ContentValues bv = new ContentValues();
                        bv.put("url_path", url);
                        bv.put("book_name", bookName);
                        bv.put("play_cnt", 0);
                        bv.put("play_pos", 0);
                        bv.put("track_idx", 0);

                        long bookId = db.insert("abooks", null, bv);

                        for (MediaTrackItem track : playlist) {
                            // create the track values object
                            ContentValues tv = new ContentValues();
                            tv.put("abook_id", bookId);
                            tv.put("url_path", track.getFileSource());
                            tv.put("title", track.getFileTitle());
                            tv.put("play_cnt", 0);
                            tv.put("play_pos", 0);
                            db.insert("abook_tracks", null, tv);
                        }

                        db.close();
                        dbHelper.close();

                        Toast.makeText(getBaseContext(), "Книга \"" + bookName + "\" успешно добавлена в базу данных книг", Toast.LENGTH_LONG).show();
                        currentActivity.finish();
                    }

                    @Override
                    public void onPlaylistDataError(String error) {
                        Toast.makeText(getBaseContext(), "Ошибка добавления книги. Возможно выбран не подходящий reader", Toast.LENGTH_LONG).show();
                        currentActivity.finish();
                    }
                });

                reader.readBookURL(url);
            }
        });

        findViewById(R.id.btnSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txtSearch = (EditText)findViewById(R.id.txtSearch);

                IBookReader reader = ((ABookReadersAdapter) readersList.getAdapter()).getCurrentReader();
                Map<String, String> criteria = new HashMap<String, String>();
                criteria.put(IBookReader.CRITERIA_CONTENT, txtSearch.getText().toString());
                reader.searchBooks(criteria);
            }
        });
    }
}
