package com.aussie.mediaapplication;

import android.content.ContentUris;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aussie.mediaapplication.core.MediaService;
import com.aussie.mediaapplication.core.MediaTrackItem;
import com.aussie.mediaapplication.core.adapters.FileSystemAdapter;
import com.aussie.mediaapplication.core.adapters.MediaTrackItemsAdapter;

import java.io.File;
import java.io.FileFilter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    final String DATA_SD = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC) + "/music.mp3";
    final Uri DATA_URI = ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, 13359);

    private MediaService mediaService;
    private EditText mediaPathEdit;
    private ListView listView;

    private File currentMediaPath = null;
    private File currentMediaFile = null;
    private FileSystemAdapter fsAdapter;

    private ImageView btPrev;
    private ImageView btNext;
    private ImageView btPP;

    private List<MediaTrackItem> playlist = new ArrayList<>();
    private MediaTrackItemsAdapter adapter;

    private SeekBar seekBar;

    private View createSnackPanelView() {
        View snackView = getLayoutInflater().inflate(R.layout.small_media_control_layout, null);

        btPrev = (ImageView) snackView.findViewById(R.id.btPrev);
        btNext = (ImageView) snackView.findViewById(R.id.btNext);
        btPP = (ImageView) snackView.findViewById(R.id.btPP);


        btPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mediaService.playlistPrev();
                } catch (Exception e) {
                    //
                }
            }
        });

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mediaService.playlistNext();
                } catch (Exception e) {

                }
            }
        });

        btPP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaService.isPlaying()) {
                    mediaService.pause();
                    btPP.setImageResource(R.mipmap.m_play);
                } else {
                    if (mediaService.isPaused()) {
                        mediaService.resume();
                        btPP.setImageResource(R.mipmap.m_pause);
                    } else {
                        if (currentMediaFile != null && currentMediaFile.length() > 0) {
                            try {
                                mediaService.playLocalAudio(currentMediaFile.getAbsolutePath(), 0);
                                btPP.setImageResource(R.mipmap.m_pause);
                            } catch (Exception e) {
                                Toast.makeText(getBaseContext(), "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });

        return snackView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mediaService = MediaService.getInstance(this);

        mediaPathEdit = (EditText) findViewById(R.id.mediaPath);
        listView = (ListView) findViewById(R.id.listView);

        seekBar = (SeekBar) findViewById(R.id.seekBar4);

//        listView.setOnItemClickListener(this);
        listView.setOnCreateContextMenuListener(this);


        final View snackView = createSnackPanelView();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create the Snackbar
                Snackbar snackbar = Snackbar.make(view, "", Snackbar.LENGTH_LONG);
                // Get the Snackbar's layout view
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                // Hide the text
                TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
                textView.setVisibility(View.INVISIBLE);

                // Show the Snackbar
                snackbar.show();
                snackbar.setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                        layout.removeAllViews();
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {
                        super.onShown(snackbar);
                        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
                        layout.addView(snackView, 0);
                    }
                });
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        currentMediaPath = new File(mediaPathEdit.getText().toString());

        final FileFilter fileFilter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (!pathname.isDirectory()) {
                    if (!pathname.getName().toLowerCase().endsWith(".mp3")) {
                        return false;
                    }
                }

                return true;
            }
        };


        if (currentMediaPath.exists() && currentMediaPath.isDirectory()) {
            List<File> files = new ArrayList<>();
            files.add(currentMediaPath.getParentFile());
            files.addAll(Arrays.asList(currentMediaPath.listFiles(fileFilter)));

            playlist.clear();
            for (File f : files) {
                playlist.add(new MediaTrackItem(f.getAbsolutePath(), f.getName(), true));
            }

            adapter = new MediaTrackItemsAdapter(getBaseContext(), playlist);
//            fsAdapter = new FileSystemAdapter(this, files);
            listView.setAdapter(adapter);
            mediaService.setPlaylist(playlist);
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MediaTrackItem track = (MediaTrackItem) parent.getAdapter().getItem(position);

                File file = new File(track.getFileSource());
                if (file.exists()) {
                    if (file.isDirectory()) {
                        currentMediaPath = file;

                        List<File> files = new ArrayList<>();
                        files.add(file.getParentFile());
                        files.addAll(Arrays.asList(currentMediaPath.listFiles(fileFilter)));

                        playlist.clear();
                        for (File f : files) {
                            playlist.add(new MediaTrackItem(f.getAbsolutePath(), f.getName(), true));
                        }

                        adapter = new MediaTrackItemsAdapter(getBaseContext(), playlist);

//                        fsAdapter = new FileSystemAdapter(getBaseContext(), files);
                        listView.setAdapter(adapter);
                        mediaService.setPlaylist(playlist);
                    } else {
                        currentMediaFile = file;

                        try {
                            mediaService.playTrack(position, 0);
                        } catch (Exception e) {
                            Toast.makeText(getBaseContext(), "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
                        }
                        btPP.setImageResource(R.mipmap.m_pause);
                    }
                }
            }
        });

        mediaService.setUpdatePosition(new MediaService.UpdatePosition() {
            @Override
            public void onPositionChanged(int duration, int currentPosition) {
                seekBar.setMax(duration);
                seekBar.setProgress(currentPosition);
            }

            @Override
            public void onTrackChanged(int trackPosition, MediaTrackItem track) {
                MediaTrackItemsAdapter adapter = ((MediaTrackItemsAdapter) listView.getAdapter());
                adapter.setCurrentIdx(trackPosition);
                adapter.notifyDataSetChanged();

                Toast.makeText(getBaseContext(), "Track name: " + track.getFileTitle(), Toast.LENGTH_LONG).show();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (fromUser) {
                    mediaService.seek(progress);
                }

//                setTimeCounterValue(txtTime1, progress);
//                setTimeCounterValue(txtTime2, mediaService.getDuration() - progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBar.setMax(mediaService.getDuration());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    public static String[] getStorageDirectories() {
        // Final set of paths
        final Set<String> rv = new HashSet<String>();
        // Primary physical SD-CARD (not emulated)
        final String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
        // All Secondary SD-CARDs (all exclude primary) separated by ":"
        final String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
        // Primary emulated SD-CARD
        final String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
        if (TextUtils.isEmpty(rawEmulatedStorageTarget)) {
            // Device has physical external storage; use plain paths.
            if (TextUtils.isEmpty(rawExternalStorage)) {
                // EXTERNAL_STORAGE undefined; falling back to default.
                rv.add("/storage/sdcard0");
            } else {
                rv.add(rawExternalStorage);
            }
        } else {
            // Device has emulated storage; external storage paths should have
            // userId burned into them.
            final String rawUserId;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                rawUserId = "";
            } else {
                final String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                final String[] folders = path.split("/");
                final String lastFolder = folders[folders.length - 1];
                boolean isDigit = false;
                try {
                    Integer.valueOf(lastFolder);
                    isDigit = true;
                } catch (NumberFormatException ignored) {
                }
                rawUserId = isDigit ? lastFolder : "";
            }
            // /storage/emulated/0[1,2,...]
            if (TextUtils.isEmpty(rawUserId)) {
                rv.add(rawEmulatedStorageTarget);
            } else {
                rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
            }
        }
        // Add all secondary storages
        if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
            // All Secondary SD-CARDs splited into array
            final String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
            Collections.addAll(rv, rawSecondaryStorages);
        }
        return rv.toArray(new String[rv.size()]);
    }

    public static HashSet<String> getExternalMounts() {
        final HashSet<String> out = new HashSet<String>();
        String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
        String s = "";
        try {
            final Process process = new ProcessBuilder().command("mount")
                    .redirectErrorStream(true).start();
            process.waitFor();
            final InputStream is = process.getInputStream();
            final byte[] buffer = new byte[1024];
            while (is.read(buffer) != -1) {
                s = s + new String(buffer);
            }
            is.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        // parse output
        final String[] lines = s.split("\n");
        for (String line : lines) {
            if (!line.toLowerCase(Locale.US).contains("asec")) {
                if (line.matches(reg)) {
                    String[] parts = line.split(" ");
                    for (String part : parts) {
                        if (part.startsWith("/"))
                            if (!part.toLowerCase(Locale.US).contains("vold"))
                                out.add(part);
                    }
                }
            }
        }
        return out;
    }


    private void showFileChooser() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        //intent.setType("*/*");      //all files
        intent.setType("text/xml");   //XML file only
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), 10);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_asbook: {
                Intent intent = new Intent(getBaseContext(), ASBookActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.nav_audiobooks: {
                Intent intent = new Intent(getBaseContext(), AudiobooksActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.nav_addbook: {
                Intent intent = new Intent(getBaseContext(), AddAudioBookActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.nav_test: {
                Intent intent = new Intent(getBaseContext(), TestActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.nav_hdkinoteatr: {
                Intent intent = new Intent(getBaseContext(), Main2Activity.class);
                startActivity(intent);
                break;
            }
            case R.id.nav_mplayer: {
                Intent intent = new Intent(getBaseContext(), MPLayerActivity.class);
                startActivity(intent);
                break;
            }
        }


        if (id == R.id.nav_asbook) {


        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.file_selector_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }
}
