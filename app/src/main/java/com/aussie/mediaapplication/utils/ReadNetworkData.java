package com.aussie.mediaapplication.utils;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by andrey.kondratyev on 02.03.2016.
 */
public class ReadNetworkData extends AsyncTask<Object, Void, Object[]> {
    private String requestMethod = "GET";
    private Map<String, String> postParams;
    private Map<String, String> urlParams;

    public interface IDataReady {
        void onDataReady(String data, Object sender);
    }

    final StringBuffer result = new StringBuffer();
    private IDataReady onDataReadyEvent;

    public ReadNetworkData() {
        postParams = new HashMap<>();
        postParams.put("USER-AGENT", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:44.0) Gecko/20100101 Firefox/44.0");
        postParams.put("ACCEPT-LANGUAGE", "en-US,en;q=0.5");
    }

    @Override
    protected Object[] doInBackground(Object... params) {
        try {
            // Create a URL for the desired page
            URL url = new URL((String) params[0]);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(requestMethod);

            if (postParams != null) {
                for (String key : postParams.keySet()) {
                    connection.setRequestProperty(key, postParams.get(key));
                }
            }

            if (!requestMethod.equals("GET")) {
                connection.setDoOutput(true);  // You need to set it to true if you want to send (output) a request body, for example with POST or PUT requests. Sending the request body itself is done via the connection's output stream:
                connection.setDoInput(true);
                connection.setChunkedStreamingMode(0);

                StringBuilder urlParameters = new StringBuilder();
                String urlConnectorChar = "";
                for (String key : urlParams.keySet()) {
                    urlParameters.append(urlConnectorChar);
                    urlParameters.append(key);
                    urlParameters.append("=");
                    urlParameters.append(urlParams.get(key));
                    urlConnectorChar = "&";
                }

                int contentLenght = urlParameters.length();
                connection.setRequestProperty("Content-Length", String.valueOf(contentLenght));

                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParameters.toString());
                dStream.flush();
                dStream.close();

                int responseCode = connection.getResponseCode();

                System.out.println("responseCode: " + responseCode);
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));


            // Read all the text returned by the server
            //BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String str;

            while ((str = in.readLine()) != null) {
                result.append(str);
            }

            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return new Object[]{result.toString(), params[1]};
    }

    @Override
    protected void onPostExecute(Object[] str) {

        if (onDataReadyEvent != null) {
            onDataReadyEvent.onDataReady((String) str[0], str[1]);
        }
    }

    public void setOnDataReadyEvent(IDataReady onIDataReadyEvent) {
        this.onDataReadyEvent = onIDataReadyEvent;
    }

    public void requestData(String url, Object sender) {
        this.execute(url, sender);
    }

    public void requestDataByPost(String url, Object sender, Map<String, String> requestParams, Map<String, String> params) {
        requestMethod = "POST";
        setRequestParams(requestParams);
        urlParams = params != null ? params : new HashMap<String, String>();
        execute(url, sender);
    }

    public void setRequestParams(Map<String, String> requestParams) {
        if (requestParams != null) {
            if (postParams != null) {
                postParams.putAll(requestParams);
            } else {
                postParams = requestParams;
            }
        } else {
            postParams = new HashMap<>();
        }
    }
}
