package com.aussie.mediaapplication.utils;

/**
 * Created by andrey.kondratyev on 05.03.2016.
 */
public class StringUtils {
    public static String unescapeJava(String escaped) {
        if (!escaped.contains("\\u")) {
            return escaped;
        }

        StringBuilder processed = new StringBuilder();

        int position = escaped.indexOf("\\u");
        while (position != -1) {
            if (position != 0)
                processed.append(escaped.substring(0, position));
            String token = escaped.substring(position + 2, position + 6);
            escaped = escaped.substring(position + 6);
            processed.append((char) Integer.parseInt(token, 16));
            position = escaped.indexOf("\\u");
        }
        processed.append(escaped);

        return processed.toString().replaceAll("(\\\\/)", "/");
    }
}
