package com.aussie.mediaapplication.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by andrey.kondratyev on 24.02.2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    final String LOG_TAG = "MediaCenter";
    final static String DB_NAME = "media.db";

    final static int DB_VERSION = 2;

    public DBHelper(Context context) {

        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "---- onCreate Database ----");

//        db.execSQL("create table trip(id integer primary key autoincrement, pprid integer, lac integer, cellid integer, gpstime numeric, lat numeric, lon numeric, alt numeric, speed numeric, status integer, provider integer);");
        db.execSQL("create table englishpod_files(_id integer primary key autoincrement, path text, filename text, playcnt integer, playpos integer, status integer, level text, descr text);");

        db.execSQL("create table abooks(_id integer primary key autoincrement, url_path text, book_name text, play_cnt integer, play_pos integer, track_idx integer);");
        db.execSQL("create table abook_tracks(_id integer primary key autoincrement, abook_id integer, url_path text, title text, play_cnt integer, play_pos integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(LOG_TAG, "---- onUpdate Database ----");
        db.execSQL("drop table englishpod_files;");
        db.execSQL("create table englishpod_files(id integer primary key autoincrement, path text, filename text, playcnt integer, playpos integer, status integer, level text, descr text);");

        db.execSQL("drop table abooks;");
        db.execSQL("drop table abook_tracks;");
        db.execSQL("create table abooks(_id integer primary key autoincrement, url_path text, book_name text, play_cnt integer, play_pos integer, track_idx integer);");
        db.execSQL("create table abook_tracks(_id integer primary key autoincrement, abook_id integer, url_path text, title text, play_cnt integer, play_pos integer);");
    }
}

