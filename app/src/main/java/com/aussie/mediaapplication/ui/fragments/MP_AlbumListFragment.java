package com.aussie.mediaapplication.ui.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.aussie.mediaapplication.R;
import com.aussie.mediaapplication.core.adapters.MediaLibraryPlaylistAdapter;
import com.aussie.mediaapplication.utils.DBHelper;

/**
 * Created by andrey.kondratyev on 11.03.2016.
 */
public class MP_AlbumListFragment extends Fragment {
    ListView albumsList;
    MediaLibraryPlaylistAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mp_album_list, container, false);

        albumsList = (ListView) view.findViewById(R.id.albumsList);


        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor c = db.query("abooks", null, null, null, null, null, null);

        adapter = new MediaLibraryPlaylistAdapter(getContext(), c, 0);
        albumsList.setAdapter(adapter);

//        adapter.setCurrentIdx(0);


        return view;
    }

    @Override
    public void onStop() {
        super.onStop();

        if(adapter != null && !adapter.getCursor().isClosed()) {
            adapter.getCursor().close();
        }
    }
}
