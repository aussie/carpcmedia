package com.aussie.mediaapplication.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.aussie.mediaapplication.AudiobooksActivity;
import com.aussie.mediaapplication.R;
import com.aussie.mediaapplication.core.adapters.MediaTrackItemsAdapter;

public class PlaylistFragment extends Fragment {
    private static final String ARG_PLAYLIST_ID_PARAM = "playlistIdParam";
    private Integer playlistId;

    private ListView trackList;

    public PlaylistFragment() {
        // Required empty public constructor
    }

    public static PlaylistFragment newInstance(Integer playlistId) {
        PlaylistFragment fragment = new PlaylistFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PLAYLIST_ID_PARAM, playlistId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            playlistId = getArguments().getInt(ARG_PLAYLIST_ID_PARAM);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AudiobooksActivity activity = (AudiobooksActivity) getActivity();

        View view = inflater.inflate(R.layout.fragment_playlist, container, false);


        trackList = (ListView) view.findViewById(R.id.trackListView);

        trackList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        return view;
    }


    public void onsetData(int bookId) {
        super.onResume();

        if(trackList==null) return;
        AudiobooksActivity activity = (AudiobooksActivity) getActivity();
//        int bookId = activity.getCurrentBookId();
        MediaTrackItemsAdapter adapter = new MediaTrackItemsAdapter(getContext(), activity.readTrackList(bookId));
        trackList.setAdapter(adapter);

    }
}
