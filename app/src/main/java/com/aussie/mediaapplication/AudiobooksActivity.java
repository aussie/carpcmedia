package com.aussie.mediaapplication;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.aussie.mediaapplication.core.MediaTrackItem;
import com.aussie.mediaapplication.core.PlaylistItem;
import com.aussie.mediaapplication.ui.fragments.MP_AlbumListFragment;
import com.aussie.mediaapplication.ui.fragments.MP_PlayListFragment;
import com.aussie.mediaapplication.ui.fragments.PlaylistFragment;
import com.aussie.mediaapplication.utils.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class AudiobooksActivity extends AppCompatActivity {
    private DBHelper dbHelper;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private int currentBookId = -1;


    public List<PlaylistItem> readPlaylists() {
        List<PlaylistItem> playlists = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = null;
        try {
            c = db.query("abooks", null, null, null, null, null, null);

            if (c.moveToFirst()) {
                int colId = c.getColumnIndex("id");
                int colUrlPath = c.getColumnIndex("url_path");
                int colBookName = c.getColumnIndex("book_name");
                int colPlayCnt = c.getColumnIndex("play_cnt");
                int colPlayPos = c.getColumnIndex("play_pos");
                int colTrackIdx = c.getColumnIndex("track_idx");

                do {
                    PlaylistItem pl = new PlaylistItem();
                    pl.setId(c.getInt(colId));
                    pl.setPath(c.getString(colUrlPath));
                    pl.setTitle(c.getString(colBookName));
                    pl.setPlay_cnt(c.getInt(colPlayCnt));
                    pl.setPlay_pos(c.getInt(colPlayPos));
                    pl.setTrack_idx(c.getInt(colTrackIdx));

                    playlists.add(pl);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "Error: " + e.toString(), Toast.LENGTH_LONG).show();
        } finally {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        }

        db.close();
        dbHelper.close();

        return playlists;
    }

    public List<MediaTrackItem> readTrackList(int abookId) {
        List<MediaTrackItem> tracks = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = null;
        try {
            c = db.query("abook_tracks", new String[]{"url_path", "title"}, "abook_id=" + abookId, null, null, null, null);

            if (c.moveToFirst()) {
                int colUrlPath = c.getColumnIndex("url_path");
                int colTitle = c.getColumnIndex("title");

                do {
                    MediaTrackItem track = new MediaTrackItem();
                    track.setFileSource(c.getString(colUrlPath));
                    track.setFileTitle(c.getString(colTitle));
                    track.setLocalResource(false);

                    tracks.add(track);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "Error: " + e.toString(), Toast.LENGTH_LONG).show();
        } finally {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        }

        db.close();
        dbHelper.close();

        return tracks;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        dbHelper = new DBHelper(getBaseContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audiobooks);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager)findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                mViewPager.
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//
//        PlaylistAdapter adapter = new PlaylistAdapter(getBaseContext(), readPlaylists());
//
//        audiobooksList = (ListView) findViewById(R.id.audiobooksList);
//        audiobooksList.setAdapter(adapter);
//        audiobooksList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                PlaylistAdapter adapter = (PlaylistAdapter) parent.getAdapter();
//
//                adapter.setCurrentIdx(position);
//                adapter.notifyDataSetChanged();
//
//                PlaylistItem playlist = adapter.getItem(position);
//
//                MediaTrackItemsAdapter trackAdapter = new MediaTrackItemsAdapter(getBaseContext(), readTrackList(playlist.getId()));
//
//                audiobooksList.setAdapter(trackAdapter);
//
//            }
//        });

    }

    public void showBooksPage() {
        mViewPager.setCurrentItem(0, true);
    }

    public void showPlaylistPage(int bookId) {
        this.currentBookId = bookId;
        mViewPager.setCurrentItem(1, true);
    }

    public int getCurrentBookId() {
        return currentBookId;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
//                return BooksListFragment.newInstance("1" + position, "2");
                return null;
            } else {
                PlaylistFragment f = PlaylistFragment.newInstance(1);
                f.onsetData(currentBookId);

                return f;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
            }
            return null;
        }
    }
}
