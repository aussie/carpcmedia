package com.aussie.mediaapplication.core.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aussie.mediaapplication.ui.fragments.MP_AlbumListFragment;
import com.aussie.mediaapplication.ui.fragments.MP_PlayListFragment;

/**
 * Created by andrey.kondratyev on 11.03.2016.
 */
public class MPLayerPagerAdapter extends FragmentPagerAdapter {
    private Context context;

    public MPLayerPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new MP_AlbumListFragment();
            case 1:
                return new MP_PlayListFragment();

        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SECTION 1";
            case 1:
                return "SECTION 2";
        }
        return null;
    }
}