package com.aussie.mediaapplication.core.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by andrey.kondratyev on 11.03.2016.
 */
public class MediaLibraryPlaylistAdapter extends CursorAdapter {
    private final LayoutInflater mInflater;
    private int currentIdx = -1;

    public MediaLibraryPlaylistAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);

        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return mInflater.inflate(android.R.layout.simple_list_item_1, null, true);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
        view.setBackgroundColor(context.getResources().getColor(android.R.color.background_light));

        text1.setText(cursor.getString(cursor.getColumnIndex("book_name")));

        if(currentIdx == cursor.getPosition()) {
            view.setBackgroundColor(context.getResources().getColor(android.R.color.holo_orange_dark));
        }
    }

    public void setCurrentIdx(int idx) {
        this.currentIdx = idx;
    }
}
