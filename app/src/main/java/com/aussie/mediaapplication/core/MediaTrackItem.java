package com.aussie.mediaapplication.core;

/**
 * Created by andrey.kondratyev on 04.03.2016.
 */
public class MediaTrackItem {
    private String fileSource;
    private String fileTitle;
    private boolean isLocalResource;

    public MediaTrackItem() {
    }

    public MediaTrackItem(String fileSource, String fileTitle, boolean isLocalResource) {
        this.fileSource = fileSource;
        this.fileTitle = fileTitle;
        this.isLocalResource = isLocalResource;
    }

    public String getFileSource() {
        return fileSource;
    }

    public void setFileSource(String fileSource) {
        this.fileSource = fileSource;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public boolean isLocalResource() {
        return isLocalResource;
    }

    public void setLocalResource(boolean localResource) {
        isLocalResource = localResource;
    }
}
