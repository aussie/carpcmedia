package com.aussie.mediaapplication.core.abooks;

import android.graphics.Bitmap;
import android.media.Image;

import com.aussie.mediaapplication.core.MediaTrackItem;

import java.util.List;
import java.util.Map;

/**
 * Created by andrey.kondratyev on 05.03.2016.
 */
public interface IBookReader {
    static final String LOG_TAG = "BookReader";
    static final String CRITERIA_CONTENT = "content";

    interface IPlaylistDataReady {
        void onPlaylistDataReady(String bookName, String bookGenre, String bookDescription, String bookCoverUrl, List<MediaTrackItem> playlist);

        void onPlaylistDataError(String error);
    }

    String getReaderName();

    void readBookURL(String url);

//    void readContents();

//    void readAuthors();

//    void readCategories();

    void searchBooks(Map<String, String> criteria);

    void setOnDataReady(IPlaylistDataReady onDataReady);
}
