package com.aussie.mediaapplication.core.abooks;

import android.util.Log;

import com.aussie.mediaapplication.core.MediaTrackItem;
import com.aussie.mediaapplication.utils.ReadNetworkData;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

/**
 * Created by andrey.kondratyev on 05.03.2016.
 */
public class ASBookReader implements IBookReader, ReadNetworkData.IDataReady {
    private IPlaylistDataReady onDataReady;
    private String bookName = "";
    private String bookGenre = "";
    private String bookDescription = "";
    private String bookCoverUrl = "";
    private String bookId = "";

    private final static String tracksBaseUrl = "http://asbook.net/getpltracks/";
    private final static String bookIdTemplate = ".*?(\\d++)-.*html";
    private final static String bookNameTemplate = "(.*?<h1 class=\".*?\">\\s*+)(.*?)(\\s*+</h1>.*)";

    private final static String coverImgTemplate = "(.*<div class=\"b-searchpost__cover\".*src=\")(.*?)(\".*)";
    private final static String genreTemplate = "(<div class=\"b-fullpost\".*?<a.*?<a.*?>)(.*?)(<.*)";
    private final static String descriptionTemplate = "(.*<div class=\"b-searchpost__text\">\\s*+\\n*+)(.*?)(</div.*)";


    @Override
    public String getReaderName() {
        return "asbook.net";
    }

    @Override
    public void readBookURL(String url) {
        ReadNetworkData reader = new ReadNetworkData();
        reader.setOnDataReadyEvent(this);
        reader.requestData(url, 1);

        bookId = url.replaceFirst(bookIdTemplate, "$1");
    }

    @Override
    public void setOnDataReady(IPlaylistDataReady onDataReady) {
        this.onDataReady = onDataReady;
    }

    @Override
    public void onDataReady(String data, Object sender) {
        switch ((int) sender) {
            case 1: {
                if (data.matches(bookNameTemplate)) {
                    bookName = data.replaceFirst(bookNameTemplate, "$2");
                }
                if (data.matches(genreTemplate)) {
                    bookGenre = data.replaceFirst(genreTemplate, "$2");
                }
                if (data.matches(descriptionTemplate)) {
                    bookDescription = data.replaceFirst(descriptionTemplate, "$2");
                }
                if (data.matches(coverImgTemplate)) {
                    bookCoverUrl = data.replaceFirst(coverImgTemplate, "$2");
                }

                String extra = Integer.toString(new Random().nextInt(10) + 1); // generate numbers from 1 to 10. Not sure if shit this need or not
                ReadNetworkData reader = new ReadNetworkData();
                reader.setOnDataReadyEvent(this);
                reader.requestData(tracksBaseUrl + bookId + "/" + extra, 2);

                break;
            }
            case 2: {
                try {
                    JSONArray mediaFileItems = new JSONObject(data).getJSONArray("playlist");

                    List<MediaTrackItem> playlist = new ArrayList<>();
                    for (int i = 0; i < mediaFileItems.length(); i++) {
                        JSONObject track = mediaFileItems.getJSONObject(i);
                        playlist.add(new MediaTrackItem(track.getString("file"), track.getString("comment"), false));
                    }

                    if (onDataReady != null) {
                        onDataReady.onPlaylistDataReady(bookName, bookGenre, bookDescription, bookCoverUrl, playlist);
                    }
                } catch (Exception e) {
                    Log.e(LOG_TAG + ".ASBook", "Error during playlist reading.", e);
                    if (onDataReady != null) {
                        onDataReady.onPlaylistDataError(e.toString());
                    }
                }

                break;
            }
            case 3: {
                if (data.matches(".*<div class=\"b-searchpost\">.*")) {

                    String[] parts = data.split("<div class=\"b-searchpost\">");

                    if (parts.length < 2) {
                        return; // no data found
                    }

                    String template = ".*b-searchpost__title.*?href=\\\"(.*?)\\\".*?>(.*?)</a>.*";

                    List<Map<String, String>> foundedData = new ArrayList<>();
                    boolean firstPass = true;

                    for (String part : parts) {
                        // skip first iteration
                        if(firstPass) {
                            firstPass = false;
                            continue;
                        }

                        String url = part.replaceFirst(template, "$1");
                        String descr = part.replaceFirst(template, "$2");

                        Map<String, String> dataRow = new HashMap<>();
                        dataRow.put("url", url);
                        dataRow.put("description", descr);

                        foundedData.add(dataRow);
                    }

                    System.out.println(foundedData);

                }
                break;
            }
        }

    }

    @Override
    public void searchBooks(Map<String, String> criteria) {
        String contentValue = criteria.get(CRITERIA_CONTENT);

        Map<String, String> params = new HashMap<>();
        params.put("do", "search");
        params.put("subaction", "search");
//        params.put("search_start", "1");
//        params.put("full_search", "0");
//        params.put("result_from", "1");

        try {
            params.put("story", URLEncoder.encode(contentValue, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Map<String, String> headerParam = new HashMap<>();
        headerParam.put("Content-Type", "application/x-www-form-urlencoded");

        ReadNetworkData reader = new ReadNetworkData();
        reader.setOnDataReadyEvent(this);
        reader.requestDataByPost("http://asbook.net", 3, headerParam, params);
    }
}
