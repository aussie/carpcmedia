package com.aussie.mediaapplication.core;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by andrey.kondratyev on 23.02.2016.
 */
public class MediaService {
    private final static String LOG_TAG = "MediaService";

    public interface UpdatePosition {
        void onPositionChanged(int duration, int currentPosition);

        void onTrackChanged(int trackPosition, MediaTrackItem track);
    }

    public enum MediaPlayerStatus {
        STOP, PLAY, PAUSE_ON_CALL, PAUSE
    }

    Timer timer;
    TimerTask tTask;
    long timerInterval = 1000;

    private static volatile MediaService mService;
    private Context context;
    private MediaPlayer mediaPlayer;
    private UpdatePosition updatePosition;
    private AudioManager am;

    private int currentPlaylistPosition = -1;

    private PhoneStateListener stateListener;
    private TelephonyManager phoneService;

    private final List<MediaTrackItem> playlist = new ArrayList<>();
    private boolean automaticPlayNext = true;

    public MediaPlayerStatus status;

    public static MediaService getInstance(Context context) {
        if (mService == null) {
            synchronized (MediaService.class) {
                mService = new MediaService(context);
            }
        }

        return mService;
    }

    private MediaService(Context context) {
        this.context = context;
        am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        timer = new Timer();

        stateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    case TelephonyManager.CALL_STATE_RINGING:
                        pause();
                        status = MediaPlayerStatus.PAUSE_ON_CALL;
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        if (status.equals(MediaPlayerStatus.PAUSE_ON_CALL)) {
                            resume();
                        }
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        pause();
                        status = MediaPlayerStatus.PAUSE_ON_CALL;
                        break;
                }

                super.onCallStateChanged(state, incomingNumber);
            }
        };

        final BroadcastReceiver mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                    Toast.makeText(context, "Bluetooth connected. Addr: " + device.getAddress(), Toast.LENGTH_LONG).show();
                } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                    Toast.makeText(context, "Bluetooth disconnected. Addr: " + device.getAddress(), Toast.LENGTH_LONG).show();
                }
            }
        };

        IntentFilter filter1 = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        IntentFilter filter3 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        context.registerReceiver(mReceiver, filter1);
        context.registerReceiver(mReceiver, filter3);
    }

    public void startPhoneStateMonitoring() {
        phoneService = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (phoneService != null) {
            phoneService.listen(stateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    public void stopPhoneStateMonitoring() {
        if (phoneService != null) {
            phoneService.listen(stateListener, PhoneStateListener.LISTEN_NONE);
        }
    }

    private void startUpdatePositionNotifier() {
        if (tTask != null) tTask.cancel();
        if (timerInterval > 0 && mediaPlayer != null && updatePosition != null && mediaPlayer.isPlaying()) {
            tTask = new TimerTask() {
                @Override
                public void run() {
                    updatePosition.onPositionChanged(getDuration(), getCurrentPosition());
                }
            };

            timer.schedule(tTask, 1000, timerInterval);
        }
    }

    private void stopUpdatePositionNotifier() {
        if (tTask != null) tTask.cancel();
    }

    public void playLocalAudio(String fileName, int seekPosition) throws Exception {
        releaseMP();

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(fileName);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.prepare();
        mediaPlayer.start();
        mediaPlayer.seekTo(seekPosition);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (automaticPlayNext && playlist.size() > currentPlaylistPosition) {
                    try {
                        currentPlaylistPosition++;
                        playTrack(currentPlaylistPosition, 0);
                    } catch (MediaServiceException e) {
                        //
                    }
                }
            }
        });

        status = MediaPlayerStatus.PLAY;
        startUpdatePositionNotifier();
    }

    public void playStreamAudio(String streamUrl, final int seekPosition) throws Exception {
        releaseMP();

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(streamUrl);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mp.seekTo(seekPosition);
                startUpdatePositionNotifier();
                status = MediaPlayerStatus.PLAY;
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (automaticPlayNext && playlist.size() > currentPlaylistPosition) {
                    try {
                        currentPlaylistPosition++;
                        playTrack(currentPlaylistPosition, 0);
                    } catch (MediaServiceException e) {
                        //
                    }
                }
            }
        });
        mediaPlayer.prepareAsync();
    }

    public void playStreamVideo(String streamUrl, SurfaceView surfaceView, int width, int height, final int seekPosition) throws Exception {
        releaseMP();

        SurfaceHolder surfaceHolder = surfaceView.getHolder();

        surfaceHolder.setFixedSize(width, height);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

//        surfaceView.setLayoutParams(new LinearLayout.LayoutParams(width, height));


        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDisplay(surfaceHolder);
        mediaPlayer.setDataSource(streamUrl);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mp.seekTo(seekPosition);
                startUpdatePositionNotifier();
                status = MediaPlayerStatus.PLAY;
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (automaticPlayNext && playlist.size() > currentPlaylistPosition) {
                    try {
//                        currentPlaylistPosition++;
                        playTrack(currentPlaylistPosition, 0);
                    } catch (MediaServiceException e) {
                        //
                    }
                }
            }
        });
        mediaPlayer.prepareAsync();
    }

    public boolean isPlaying() {
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    public boolean isPaused() {
        return mediaPlayer != null && status.equals(MediaPlayerStatus.PAUSE) | status.equals(MediaPlayerStatus.PAUSE_ON_CALL) | status.equals(MediaPlayerStatus.STOP);
    }

    public void pause() {
        if (isPlaying()) {
            mediaPlayer.pause();
            status = MediaPlayerStatus.PAUSE;
            stopUpdatePositionNotifier();
        }
    }

    public void resume() {
        if (isPaused()) {
            mediaPlayer.start();
            startUpdatePositionNotifier();
        }
    }

    public void stop() {
        releaseMP();
    }


    private void releaseMP() {
        stopPhoneStateMonitoring();
        stopUpdatePositionNotifier();
        status = MediaPlayerStatus.STOP;

        if (mediaPlayer != null) {
            try {
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Set playlist for playing
     *
     * @param playlist
     */
    public void setPlaylist(List<MediaTrackItem> playlist) {
        if (playlist == null || playlist.size() == 0) {
            return;
        }

        this.playlist.clear();
        this.playlist.addAll(playlist);
    }

    public void playTrack(int trackPosition, int trackSeekPosition) throws MediaServiceException {
        currentPlaylistPosition = trackPosition;

        try {
            MediaTrackItem track = playlist.get(trackPosition);
            if (track.isLocalResource()) {
                playLocalAudio(track.getFileSource(), trackSeekPosition);
            } else {
                playStreamAudio(track.getFileSource(), trackSeekPosition);
            }

            if (updatePosition != null) {
                updatePosition.onTrackChanged(currentPlaylistPosition, track);
            }

            startPhoneStateMonitoring();
        } catch (Exception e) {
            Log.d(LOG_TAG, "Error playing file. " + e);
        }

    }

    public void playlistNext() throws MediaServiceException {
        if (playlist.size() > 0 && currentPlaylistPosition > -1 && currentPlaylistPosition < playlist.size()) {
            currentPlaylistPosition++;

            playTrack(currentPlaylistPosition, 0);
        }
    }

    public void playlistPrev() throws MediaServiceException {
        if (playlist.size() > 0 && currentPlaylistPosition > -1) {
            currentPlaylistPosition--;

            playTrack(currentPlaylistPosition, 0);
        }
    }

    public int getDuration() {
        if (mediaPlayer != null) {
            return mediaPlayer.getDuration();
        }

        return -1;
    }

    public int getCurrentPosition() {
        if (mediaPlayer != null) {
            return mediaPlayer.getCurrentPosition();
        }

        return -1;
    }

    public void seek(int position) {
        if (mediaPlayer != null) {
            mediaPlayer.seekTo(position);
        }
    }

    public void setUpdatePosition(UpdatePosition updatePosition) {
        this.updatePosition = updatePosition;
    }
}
