package com.aussie.mediaapplication.core.abooks;

import android.util.Log;

import com.aussie.mediaapplication.core.MediaTrackItem;
import com.aussie.mediaapplication.utils.ReadNetworkData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by andrey.kondratyev on 05.03.2016.
 */
public class AudioknigiClubReader implements IBookReader, ReadNetworkData.IDataReady {
    private IPlaylistDataReady onDataReady;
    private String bookName = "";
    private String bookGenre = "";
    private String bookDescription = "";
    private String bookCoverUrl = "";


    private final static String tracksBaseUrl = "http://audioknigi.club/rest/bid/";
    private final static String bookIdTemplate = "(.*?audioPlayer\\()(.*?)(,.*)";
    private final static String bookNameTemplate = "(.*?<h1 class=\".*?\">\\s*+)(.*?)(\\s*+</h1>.*)";

    private final static String coverImgTemplate = "(.*class=\"picture-side.*?img src=\")(.*?)(\".*)";
    private final static String genreTemplate = "(.*<div class=\"topic-blog\">.*?href.*?>)(.*?)(</a>.*)";
    private final static String descriptionTemplate = "(.*<div class=\"topic-content text text-inside\">\\s*+\\n*+)(.*?)(</div>.*)";


    @Override
    public String getReaderName() {
        return "audioknigi.club";
    }

    @Override
    public void readBookURL(String url) {
        ReadNetworkData reader = new ReadNetworkData();
        reader.setOnDataReadyEvent(this);
        reader.requestData(url, 1);
    }

    @Override
    public void searchBooks(Map<String, String> criteria) {

    }

    @Override
    public void setOnDataReady(IPlaylistDataReady onDataReady) {
        this.onDataReady = onDataReady;
    }

    @Override
    public void onDataReady(String data, Object sender) {
        try {
            switch ((int) sender) {
                case 1:
                    String url = tracksBaseUrl + data.replaceFirst(bookIdTemplate, "$2");

                    ReadNetworkData reader = new ReadNetworkData();
                    reader.setOnDataReadyEvent(this);
                    reader.requestData(url, 2);

                    if (data.matches(bookNameTemplate)) {
                        bookName = data.replaceFirst(bookNameTemplate, "$2");
                    }
                    if (data.matches(genreTemplate)) {
                        bookGenre = data.replaceFirst(genreTemplate, "$2");
                    }
                    if (data.matches(descriptionTemplate)) {
                        bookDescription = data.replaceFirst(descriptionTemplate, "$2");
                    }
                    if (data.matches(coverImgTemplate)) {
                        bookCoverUrl = "http://knigogolik.com" + data.replaceFirst(coverImgTemplate, "$2");
                    }
                    break;
                case 2:
                    try {
                        JSONArray mediaFileItems = new JSONObject("{\"playlist\":" + data + "}").getJSONArray("playlist");

                        List<MediaTrackItem> playlist = new ArrayList<>();
                        for (int i = 0; i < mediaFileItems.length(); i++) {
                            JSONObject track = mediaFileItems.getJSONObject(i);
                            playlist.add(new MediaTrackItem(track.getString("mp3"), track.getString("title"), false));
                        }

                        if (onDataReady != null) {
                            onDataReady.onPlaylistDataReady(bookName, bookGenre, bookDescription, bookCoverUrl, playlist);
                        }
                    } catch (Exception e) {
                        Log.e(LOG_TAG + ".Audioknigi", "Error during playlist reading.", e);
                        if (onDataReady != null) {
                            onDataReady.onPlaylistDataError(e.toString());
                        }
                    }

                    break;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG + ".Audioknigi", "Error during playlist reading.", e);
            if (onDataReady != null) {
                onDataReady.onPlaylistDataError(e.toString());
            }
        }


    }
}
