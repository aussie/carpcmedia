package com.aussie.mediaapplication.core.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aussie.mediaapplication.core.MediaTrackItem;

import java.util.List;

/**
 * Created by andrey.kondratyev on 04.03.2016.
 */
public class MediaTrackItemsAdapter extends ArrayAdapter<MediaTrackItem> {
    private final LayoutInflater mInflater;
    private int currentIdx = -1;

    public MediaTrackItemsAdapter(Context context, List<MediaTrackItem> data) {
        super(context, android.R.layout.simple_list_item_1, data);
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(android.R.layout.simple_list_item_1, null, true);
        }

        convertView.setBackgroundColor(getContext().getResources().getColor(android.R.color.background_light));

        TextView text = ((TextView) convertView.findViewById(android.R.id.text1));
        MediaTrackItem item = getItem(position);

        text.setText(item.getFileTitle());

        if(currentIdx == position) {
            text.setBackgroundColor(getContext().getResources().getColor(android.R.color.holo_orange_dark));
        }

        return convertView;
    }

    public void setCurrentIdx(int idx) {
        this.currentIdx = idx;
    }
}
