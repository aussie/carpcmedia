package com.aussie.mediaapplication.core;

/**
 * Created by andrey.kondratyev on 05.03.2016.
 */
public class PlaylistItem {
    int id;
    String title;
    String path;
    int play_cnt;
    int play_pos;
    int track_idx;
    boolean isUrl;

    public PlaylistItem() {
    }

    public PlaylistItem(int id, String title, String path, int play_cnt, int play_pos, int track_idx, boolean isUrl) {
        this.id = id;
        this.title = title;
        this.path = path;
        this.play_cnt = play_cnt;
        this.play_pos = play_pos;
        this.track_idx = track_idx;
        this.isUrl = isUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getPlay_cnt() {
        return play_cnt;
    }

    public void setPlay_cnt(int play_cnt) {
        this.play_cnt = play_cnt;
    }

    public int getPlay_pos() {
        return play_pos;
    }

    public void setPlay_pos(int play_pos) {
        this.play_pos = play_pos;
    }

    public int getTrack_idx() {
        return track_idx;
    }

    public void setTrack_idx(int track_idx) {
        this.track_idx = track_idx;
    }

    public boolean isUrl() {
        return isUrl;
    }

    public void setUrl(boolean url) {
        isUrl = url;
    }
}
