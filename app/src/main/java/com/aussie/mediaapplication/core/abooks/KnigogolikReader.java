package com.aussie.mediaapplication.core.abooks;

import android.util.Log;

import com.aussie.mediaapplication.core.MediaTrackItem;
import com.aussie.mediaapplication.utils.ReadNetworkData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by andrey.kondratyev on 05.03.2016.
 */
public class KnigogolikReader implements IBookReader, ReadNetworkData.IDataReady {
    private IPlaylistDataReady onDataReady;
    private String bookName = "";
    private String bookGenre = "";
    private String bookDescription = "";
    private String bookCoverUrl = "";

    private static final String secondPageTemplate = "(.*?var myPlaylist_[0-9]+? = \\[\\{\\s*+)(.*?)(\\s++\\}\\];.*)";
    private static final String firstPageTemplate = "(.*onclick=\"nmapopup=window.open\\(')(.*?)('.*)";
    private static final String mp3Template = "mp3:'(.*)'.*";
    private static final String titleTemplate = "title:'(.*)'.*";
    private static final String splitMediaFilesTemplate = "[}], [{]";
    private final static String bookNameTemplate = "(.*?<h1 class=\".*?\">\\s*+)(.*?)(\\s*+</h1>.*)";

    private final static String coverImgTemplate = "(.*<span class=\"itemImage_01\">.*<img src=\")(.*?)(\".*)";
    private final static String genreTemplate = "(.*<div class=\"item_janr_4\">.*<br>)(.*?)(</div>.*)";
    private final static String descriptionTemplate = "(.*<div class=\"itemIntroText_01\">)(.*?)(</div>.*)";


    @Override
    public String getReaderName() {
        return "knigogolik.com";
    }

    @Override
    public void readBookURL(String url) {
        ReadNetworkData dataReader = new ReadNetworkData();
        dataReader.setOnDataReadyEvent(this);
        dataReader.requestData(url, 1);
    }

    @Override
    public void searchBooks(Map<String, String> criteria) {

    }

    @Override
    public void setOnDataReady(IPlaylistDataReady onDataReady) {
        this.onDataReady = onDataReady;
    }

    @Override
    public void onDataReady(String data, Object sender) {

        try {
            switch ((int) sender) {
                case 1:
                    String popupUrl = data.replaceFirst(firstPageTemplate, "$2");

                    ReadNetworkData dataReader = new ReadNetworkData();
                    dataReader.setOnDataReadyEvent(this);
                    dataReader.requestData(popupUrl, 2);

                    if (data.matches(bookNameTemplate)) {
                        bookName = data.replaceFirst(bookNameTemplate, "$2");
                    }
                    if (data.matches(genreTemplate)) {
                        bookGenre = data.replaceFirst(genreTemplate, "$2");
                    }
                    if (data.matches(descriptionTemplate)) {
                        bookDescription = data.replaceFirst(descriptionTemplate, "$2");
                    }
                    if (data.matches(coverImgTemplate)) {
                        bookCoverUrl = "http://knigogolik.com" + data.replaceFirst(coverImgTemplate, "$2");
                    }

                    break;
                case 2:
                    String[] files = data.replaceFirst(secondPageTemplate, "$2").split(splitMediaFilesTemplate);

                    List<MediaTrackItem> playlist = new ArrayList<>();
                    for (String file : files) {
                        String parts[] = file.trim().split(",");
                        String fileUrl = parts[0].replaceFirst(mp3Template, "$1");
                        String fileTitle = parts[1].trim().replaceFirst(titleTemplate, "$1");

                        playlist.add(new MediaTrackItem(fileUrl, fileTitle, false));
                    }

                    if (onDataReady != null) {
                        onDataReady.onPlaylistDataReady(bookName, bookGenre, bookDescription, bookCoverUrl, playlist);
                    }

                    break;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG + ".Knigogolik", "Error during playlist reading.", e);
            if (onDataReady != null) {
                onDataReady.onPlaylistDataError(e.toString());
            }
        }

    }
}
