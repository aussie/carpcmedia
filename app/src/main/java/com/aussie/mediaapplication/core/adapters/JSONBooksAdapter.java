package com.aussie.mediaapplication.core.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrey.kondratyev on 03.03.2016.
 */
public class JSONBooksAdapter extends ArrayAdapter<JSONObject> {
    private final LayoutInflater mInflater;

    public JSONBooksAdapter(Context context, JSONArray data) {
        super(context, android.R.layout.simple_list_item_1);
        this.mInflater = LayoutInflater.from(context);

        try {
            List<JSONObject> items = new ArrayList<>();
            for (int i = 0; i < data.length(); i++) {
                items.add(data.getJSONObject(i));
            }

            addAll(items);
        } catch (JSONException e) {
            //
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(android.R.layout.simple_list_item_1, null, true);
        }

        TextView text = ((TextView) convertView.findViewById(android.R.id.text1));
        JSONObject item = getItem(position);


        try {
            text.setText(item.getString("comment"));
        } catch (JSONException e) {
            //
        }


        return convertView;
    }

}
