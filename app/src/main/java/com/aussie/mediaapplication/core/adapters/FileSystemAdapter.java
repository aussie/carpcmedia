package com.aussie.mediaapplication.core.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.List;

/**
 * Created by andrey.kondratyev on 23.02.2016.
 */
public class FileSystemAdapter extends ArrayAdapter<File> {
    private final LayoutInflater mInflater;

    public FileSystemAdapter(Context context, File[] data) {
        super(context, android.R.layout.simple_list_item_1, data);
        this.mInflater = LayoutInflater.from(context);
    }

    public FileSystemAdapter(Context context, List<File> data) {
        super(context, android.R.layout.simple_list_item_1, data);
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(android.R.layout.simple_list_item_1, null, true);
        }

        TextView text = ((TextView) convertView.findViewById(android.R.id.text1));
        File item = getItem(position);

        if (position == 0) {
            text.setText("..");
        } else {
            text.setText(item.getName());

        }

        return convertView;
    }
}
