package com.aussie.mediaapplication;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aussie.mediaapplication.core.adapters.MPLayerPagerAdapter;

public class MPLayerActivity extends AppCompatActivity {
    private MPLayerPagerAdapter pagerAdapter;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mplayer);

        pagerAdapter = new MPLayerPagerAdapter(getBaseContext(), getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.mplayer_container);
        viewPager.setAdapter(pagerAdapter);
    }
}
