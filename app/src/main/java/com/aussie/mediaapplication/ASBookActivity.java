package com.aussie.mediaapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aussie.mediaapplication.core.MediaTrackItem;
import com.aussie.mediaapplication.core.MediaService;
import com.aussie.mediaapplication.core.MediaServiceException;
import com.aussie.mediaapplication.core.adapters.MediaTrackItemsAdapter;
import com.aussie.mediaapplication.utils.ReadNetworkData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class ASBookActivity extends AppCompatActivity implements View.OnClickListener {
    private MediaService mediaService;
    private SharedPreferences sPref;

    private EditText urlText;
    private Button readUrl;
    private ListView listView;

    private ImageView btnPrev;
    private ImageView btnPP;
    private ImageView btnNext;

    private SeekBar seekBar;
    private TextView txtTime1;
    private TextView txtTime2;

    public static final String LAST_PLAYED_FILE = "LAST_PLAYED_FILE";
    public static final String LAST_PLAYED_POSITION = "LAST_PLAYED_POSITION";


    private void loadBook(String bookUrl, final boolean isNew) {
        final int[] lastPlayedFileIdx = {0};
        final int[] lastPlayedPosition = {0};

        if (isNew) {
            SharedPreferences.Editor editor = sPref.edit();
            editor.putString("URL", urlText.getText().toString());
            editor.apply();
        } else {
            lastPlayedFileIdx[0] = sPref.getInt(LAST_PLAYED_FILE, 0);
            lastPlayedPosition[0] = sPref.getInt(LAST_PLAYED_POSITION, 0);
        }

//        String extra = Integer.toString(new Random().nextInt(2) + 1);
        String extra = "2";


        StringBuilder contentUrl = new StringBuilder("http://asbook.net/getpltracks/");
        contentUrl.append(bookUrl.toString().replaceFirst(".*?(\\d++)-.*html", "$1"));

        contentUrl.append("/" + extra); // generate numbers from 1 to 10. Not sure if shit this need or not
        contentUrl.append("/aabbccddee12345");

        ReadNetworkData reader = new ReadNetworkData();
        reader.requestData(contentUrl.toString(), 1);
        reader.setOnDataReadyEvent(new ReadNetworkData.IDataReady() {
            @Override
            public void onDataReady(String data, Object sender) {

                try {
                    JSONArray mediaFileItems = new JSONObject(data).getJSONArray("playlist");

                    List<MediaTrackItem> tracks = new ArrayList<>();
                    for (int i = 0; i < mediaFileItems.length(); i++) {
                        JSONObject track = mediaFileItems.getJSONObject(i);
                        String trackUrl = track.getString("file");
                        if (!trackUrl.startsWith("http")) {
                            trackUrl = "http://77.245.68.26/books/" + trackUrl;
                        }

                        tracks.add(new MediaTrackItem(trackUrl, track.getString("comment"), false));
                    }

                    MediaTrackItemsAdapter adapter = new MediaTrackItemsAdapter(getBaseContext(), tracks);
                    listView.setAdapter(adapter);
                    btnPP.setImageResource(R.mipmap.m_pause);

                    mediaService.setPlaylist(tracks);
                    mediaService.playTrack(lastPlayedFileIdx[0], lastPlayedPosition[0]);


//                    listView.setSelection(lastPlayedFileIdx[0]);

//                    playFile(adapter.getItem(lastPlayedFileIdx[0]), lastPlayedFileIdx[0], lastPlayedPosition[0], !isNew);
                } catch (Exception e) {
                    //
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sPref = getPreferences(MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asbook);
        mediaService = MediaService.getInstance(this);

        urlText = (EditText) findViewById(R.id.urlText);
        readUrl = (Button) findViewById(R.id.readUrl);
        listView = (ListView) findViewById(R.id.listView);
        btnPrev = (ImageView) findViewById(R.id.btnPrev);
        btnPP = (ImageView) findViewById(R.id.btnPP);
        btnNext = (ImageView) findViewById(R.id.btnNext);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        txtTime1 = (TextView) findViewById(R.id.txtTime1);
        txtTime2 = (TextView) findViewById(R.id.txtTime2);

        btnPrev.setOnClickListener(this);
        btnPP.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        String url = sPref.getString("URL", urlText.getText().toString());
        urlText.setText(url);

        loadBook(url, false);

        readUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadBook(urlText.getText().toString(), true);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    mediaService.playTrack(position, 0);
                } catch (MediaServiceException e) {
                    e.printStackTrace();
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SharedPreferences.Editor editor = sPref.edit();
                editor.putInt(LAST_PLAYED_POSITION, progress);
                editor.apply();

                if (fromUser) {
                    mediaService.seek(progress);
                }

                setTimeCounterValue(txtTime1, progress);
                setTimeCounterValue(txtTime2, mediaService.getDuration() - progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBar.setMax(mediaService.getDuration());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // update position during playing
        mediaService.setUpdatePosition(new MediaService.UpdatePosition() {
            @Override
            public void onPositionChanged(int duration, int currentPosition) {
                seekBar.setMax(duration);
                seekBar.setProgress(currentPosition);
            }

            @Override
            public void onTrackChanged(int trackPosition, MediaTrackItem track) {
                MediaTrackItemsAdapter adapter = ((MediaTrackItemsAdapter) listView.getAdapter());
                adapter.setCurrentIdx(trackPosition);
                adapter.notifyDataSetChanged();

                SharedPreferences.Editor editor = sPref.edit();
                editor.putInt(LAST_PLAYED_FILE, trackPosition);
                editor.apply();


                Toast.makeText(getBaseContext(), "Track name: " + track.getFileTitle(), Toast.LENGTH_LONG).show();
            }
        });


        // handling BT media buttons
        IntentFilter filter = new IntentFilter("android.intent.action.MEDIA_BUTTON");
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);

        BroadcastReceiver mediaButtonsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                KeyEvent key = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

                switch (key.getKeyCode()) {
                    case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                        if (mediaService.isPlaying()) {
                            mediaService.pause();
                        } else if (mediaService.isPaused()) {
                            mediaService.resume();
                        }
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                        try {
                            mediaService.playlistPrev();
                        } catch (MediaServiceException e) {
                            e.printStackTrace();
                        }
                        break;
                    case KeyEvent.KEYCODE_MEDIA_NEXT:
                        try {
                            mediaService.playlistNext();
                        } catch (MediaServiceException e) {
                            e.printStackTrace();
                        }
                        break;
                }

                abortBroadcast();
            }
        };

        registerReceiver(mediaButtonsReceiver, filter);


    }

    private void setTimeCounterValue(TextView timerLabel, long time) {
        long seconds = time / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        String str = String.format(new Locale("UTF-8"), "%02d:%02d:%02d", hours % 24, minutes % 60, seconds % 60);
        timerLabel.setText(str);
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.btnPrev:
                    mediaService.playlistPrev();
                    break;
                case R.id.btnPP:
                    if (mediaService.isPlaying()) {
                        mediaService.pause();
                        btnPP.setImageResource(R.mipmap.m_play);
                    } else {
                        if (mediaService.isPaused()) {
                            mediaService.resume();
                            btnPP.setImageResource(R.mipmap.m_pause);
                        }
                    }

                    break;
                case R.id.btnNext:
                    mediaService.playlistNext();
                    break;
            }
        } catch (Exception e) {
            Log.e("ASBOOK", "Error: " + e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d("ASBOOK", "onPause");

        //todo: put save current position code here
    }
}
